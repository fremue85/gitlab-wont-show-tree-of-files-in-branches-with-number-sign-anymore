# Gitlab won't show tree of files in branches with number sign anymore

> Anyone?

## Update 2020-01-28 05:11 CET

* Seems to be resolved: <https://twitter.com/furederiqoo/status/1222190483893575680>
* Issue here: <https://gitlab.com/gitlab-org/gitlab/issues/199308>
